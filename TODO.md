### TASKS

* Use QString on (almost) everything to remove duplicate type conversions to/fromStdString (yeah it's pretty much a mess down there)

### BUGS

* \*sounds of wind and tumbleweeds rolling by\*

### IDEAS/SUGGESTIONS

* Clean up if backup process is manually cancelled
* Filter list (by size, genre, publisher/developer, year, name, etc., any other ways?) - would require changing database structure
* Find a way to optionally compress content (which format? how to predict approx. size?)